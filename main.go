package main

import (
	"fmt"
	"gitlab.com/Yu-Leo/bmstu-tower-tree/bst"
)

func Example() {
	tree := bst.NewTree()

	tree.AddItem(10)
	tree.AddItem(15)
	tree.AddItem(11)
	tree.AddItem(12)
	tree.AddItem(13)

	fmt.Println(tree.FindItem(15))
	fmt.Println(tree.FindItem(8))
	fmt.Println(tree.FindItem(10))
	fmt.Println(tree.FindItem(20))

	fmt.Println("Before deleting: ")
	tree.Print()
	fmt.Println()

	tree.DeleteItem(15)
	tree.DeleteItem(11)
	tree.DeleteItem(12)
	tree.DeleteItem(20)

	fmt.Println("After deleting: ")
	tree.Print()
	fmt.Println()
	fmt.Println(tree.FindItem(15))
	fmt.Println(tree.FindItem(8))

}

func main() {
	Example()
}
