package bst

import (
	"errors"
	"fmt"
)

type Tree struct {
	Root *Node
}

func NewTree() *Tree {
	return &Tree{}
}

func (t *Tree) FindItem(value int) bool {
	foundNode := findItem(t.Root, value)
	return foundNode != nil
}

func (t *Tree) AddItem(value int) {
	newNode := Node{value, nil, nil}
	addItem(&t.Root, &newNode)
}

func (t *Tree) DeleteItem(value int) bool { // Return status code
	err, _ := deleteItem(t.Root, value) // True - Element is deleted; False - Element not found
	if err != nil {                     // Catch err
		return false
	}
	return true
}

func (t *Tree) Print() {
	infixTraverse(t.Root)
}

type Node struct {
	Value int
	Left  *Node
	Right *Node
}

func findItem(n *Node, value int) *Node {
	if n == nil {
		return nil
	}
	if n.Value == value {
		return n
	} else if value < n.Value {
		return findItem(n.Left, value)
	} else { // value > n.Value
		return findItem(n.Right, value)
	}
}

func addItem(n **Node, newNode *Node) {
	if *n == nil {
		*n = newNode
	}
	if newNode.Value < (*n).Value {
		addItem(&(*n).Left, newNode)
	} else if newNode.Value > (*n).Value {
		addItem(&(*n).Right, newNode)
	}
}

func deleteItem(n *Node, value int) (error, *Node) {
	var err error
	if n == nil {
		return errors.New("not found"), nil
	}
	if n.Value > value {
		err, n.Left = deleteItem(n.Left, value)
	} else if n.Value < value {
		err, n.Right = deleteItem(n.Right, value)
	} else { // value == n.Value
		if n.Right == nil && n.Left == nil {
			n = nil
			return nil, n
		}
		if n.Right != nil && n.Left == nil {
			n = n.Right
			return nil, n
		}
		if n.Right == nil && n.Left != nil {
			n = n.Left
			return nil, n
		}
		temp := n.Left
		for temp != nil && temp.Left != nil {
			temp = temp.Left
		}
		n.Value = temp.Value
		err, n.Right = deleteItem(n.Right, temp.Value)
	}
	return err, n
}

func infixTraverse(n *Node) {
	if n == nil {
		return
	}
	infixTraverse(n.Left)
	fmt.Printf("%d ", n.Value)
	infixTraverse(n.Right)
}
