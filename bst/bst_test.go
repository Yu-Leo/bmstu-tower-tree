package bst

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindItem(t *testing.T) {
	// Arrange
	tree := NewTree()
	tree.Root = &Node{Value: 10}
	tree.Root.Left = &Node{Value: 5}
	tree.Root.Right = &Node{Value: 15}
	tree.Root.Right.Left = &Node{Value: 11}
	tree.Root.Right.Left.Right = &Node{Value: 12}

	testCases := []struct {
		value    int
		expected bool
	}{
		{15, true},
		{10, true},
		{11, true},
		{12, true},
		{5, true},
		{8, false},
		{0, false},
	}

	for _, testCase := range testCases {
		// Act
		result := tree.FindItem(testCase.value)

		// Assert
		assert.Equal(t, testCase.expected, result,
			fmt.Sprintf("Incorrect result for value: %d", testCase.value))

	}
}

func TestAddItem(t *testing.T) {
	// Arrange
	tree := NewTree()

	// Act
	tree.AddItem(10)
	tree.AddItem(15)
	tree.AddItem(5)
	tree.AddItem(13)

	testTable := []struct {
		n             *Node
		expectedValue int
	}{
		{tree.Root, 10},
		{tree.Root.Right, 15},
		{tree.Root.Left, 5},
		{tree.Root.Right.Left, 13},
	}

	// Assert
	for _, testCase := range testTable {
		assert.Equal(t, testCase.n.Value, testCase.expectedValue,
			fmt.Sprintf("Incorrect result for value: %d", testCase.expectedValue))
	}
}

func TestDeleteItem(t *testing.T) {
	// Arrange
	tree := NewTree()
	tree.Root = &Node{Value: 10}
	tree.Root.Left = &Node{Value: 5}
	tree.Root.Right = &Node{Value: 15}
	tree.Root.Right.Left = &Node{Value: 11}
	tree.Root.Right.Left.Right = &Node{Value: 12}

	// Act
	tree.DeleteItem(5)
	tree.DeleteItem(11)

	testTable := []struct {
		n             *Node
		expectedValue int
	}{
		{tree.Root, 10},
		{tree.Root.Right, 15},
		{tree.Root.Right.Left, 12},
	}

	// Assert
	for _, testCase := range testTable {
		assert.Equal(t, testCase.n.Value, testCase.expectedValue,
			fmt.Sprintf("Incorrect result for value: %d", testCase.expectedValue))
	}
}
